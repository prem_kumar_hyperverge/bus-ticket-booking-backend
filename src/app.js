var express = require("express");
const cors = require("cors");
var bodyParser = require("body-parser");
const userRouter = require("./router/userRouter");
const adminRouter = require("./router/adminRouter");

var app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({ limit: "50mb" }));
app.use(
  bodyParser.urlencoded({
    limit: "50mb",
    extended: true,
    parameterLimit: 50000,
  })
);
app.use(cors());

app.get("/", (req, res) => {
  res.send("Welcome to Bus Ticket Booking API");
});
app.use((err, req, res, next) => {
  res.status(err.status).send(err.data);
});

app.use("/user", userRouter);
app.use("/admin", adminRouter);
module.exports = app;
