const isValidPhoneNumber = (phoneNumber) => {
  const regEx = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
  return regEx.test(phoneNumber);
};
const validateEmail = (email) => {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
};
function validate(req, res, next) {
  if (req.originalUrl === "/admin/register") {
    const { userName, email, password, mobileNumber } = req.body;
    if (
      !userName ||
      !email ||
      !password ||
      !password.length > 5 ||
      !mobileNumber ||
      !isValidPhoneNumber(mobileNumber) ||
      !validateEmail(email)
    ) {
      return error(res);
    }
  } else if (req.originalUrl === "/admin/login") {
    const { email, password } = req.body;
    if (!email || !password || !password.length > 5 || !validateEmail(email)) {
      return error(res);
    }
  } else if (
    req.originalUrl === "/admin/resetSeats" ||
    req.originalUrl === "/admin/removeBuses" ||
    req.originalUrl === "/user/busDetails"
  ) {
    const { busId } = req.body;
    if (!busId) {
      return error(res);
    }
  } else if (req.originalUrl === "/admin/addBuses") {
    const {
      busName,
      userId,
      sourceCity,
      destinationCity,
      fare,
      travelDate,
      departureTime,
      arrivalTime,
      noOfSeats,
      busNumber,
      pickupLocation,
      dropLocation,
      busImage,
    } = req.body;
    if (
      !busName ||
      !userId ||
      !sourceCity ||
      !destinationCity ||
      !fare ||
      !travelDate ||
      !departureTime ||
      !arrivalTime ||
      !noOfSeats ||
      !busNumber ||
      !pickupLocation ||
      !dropLocation ||
      !busImage
    ) {
      return error(res);
    }
  } else if (req.originalUrl === "/admin/viewBuses") {
    const { userId } = req.body;
    if (!userId) {
      return error(res);
    }
  } else if (req.originalUrl === "/user/ticketDetails") {
    const { ticketId } = req.body;
    if (!ticketId) {
      return error(res);
    }
  } else if (req.originalUrl === "/user/bookSeats") {
    const {
      seatId,
      userName,
      email,
      mobileNumber,
      age,
      busId,
      gender,
    } = req.body;
    if (
      !seatId ||
      !userName ||
      !email ||
      !mobileNumber ||
      !age ||
      !busId ||
      !gender
    ) {
      return error(res);
    }
  } else if (req.originalUrl === "/user/getBuses") {
    const { sourceCity, destinationCity, travelDate } = req.body;
    if (!sourceCity || !destinationCity || !travelDate) {
      return error(res);
    }
  }

  next();
}
function error(res) {
  return res.status(400).send({
    message: "Bad Payload",
  });
}
module.exports = validate;
