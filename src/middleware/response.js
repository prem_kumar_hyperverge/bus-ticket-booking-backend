function response(status, data) {
  return {
    status: status,
    data: data,
  };
}

module.exports = response;
