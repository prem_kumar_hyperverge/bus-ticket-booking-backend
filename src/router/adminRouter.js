const router = require("express").Router();
var jwtToken = require("../middleware/jwtToken");
var validation = require("../middleware/validation");
const {
  resetSeats,
  removeBuses,
  viewBuses,
  addBuses,
  login,
  register,
} = require("../controllers/adminController");

router.post("/register", validation, register);
router.post("/login", validation, login);
router.post("/addBuses", jwtToken, validation, addBuses);
router.post("/viewBuses", jwtToken, validation, viewBuses);
router.post("/removeBuses", jwtToken, validation, removeBuses);
router.post("/resetSeats", jwtToken, validation, resetSeats);

module.exports = router;
