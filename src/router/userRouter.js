const router = require("express").Router();
var validation = require("../middleware/validation");
const {
  getBuses,
  bookSeats,
  ticketDetails,
  busDetail,
} = require("../controllers/userController");
const ticket = require("../model/ticket");

router.post("/getBuses", validation, getBuses);
router.post("/bookSeats", validation, bookSeats);
router.post("/ticketDetails", validation, ticketDetails);
router.post("/busDetails", validation, busDetail);

module.exports = router;
