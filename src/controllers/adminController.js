var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");
const Admin = require("../model/admin");
var response = require("../middleware/response");
const Buses = require("../model/buses");
const seatData = require("../constants/index");
const errorCode = require("../utils/errorCodes");

exports.register = async (req, res, next) => {
  const { userName, email, password, mobileNumber } = req.body;
  let admin = await Admin.findOne({
    email,
  });
  if (admin) {
    next(response(errorCode.bad_request, "User Already Exists"));
  }
  admin = new Admin({
    userName,
    email,
    password,
    mobileNumber,
  });
  admin.password = await bcrypt.hashSync(password, 10);
  await admin.save();
  const payload = admin.id;
  res.status(errorCode.ok).send(payload);
};

exports.login = async (req, res, next) => {
  const { email, password } = req.body;
  const admin = await Admin.findOne({
    email,
  });
  if (!admin) next(response(errorCode.bad_request, "User Not Exist"));

  const isMatch = bcrypt.compareSync(password, admin.password);
  if (!isMatch) next(errorCode.bad_request, "Incorrect Password !");
  const data = {
    admin: {
      id: admin.id,
      userName: admin.userName,
      email: admin.email,
    },
  };
  const payload = {
    id: admin.id,
  };
  const token = await jwt.sign(payload, process.env.jwtSecret, {
    expiresIn: 86400000 * 15, // 15 Days
  });
  res.status(errorCode.ok).json({
    token: token,
    message: "Login success :)",
    data: data,
  });
};

exports.addBuses = async (req, res, next) => {
  const {
    busName,
    userId,
    sourceCity,
    destinationCity,
    fare,
    travelDate,
    departureTime,
    arrivalTime,
    noOfSeats,
    busNumber,
    pickupLocation,
    dropLocation,
    busImage,
  } = req.body;
  let seatDetails = seatData.seatDetails();
  let buses = new Buses({
    busName,
    userId,
    seatDetails,
    sourceCity,
    destinationCity,
    fare,
    travelDate,
    departureTime,
    arrivalTime,
    noOfSeats,
    busNumber,
    pickupLocation,
    dropLocation,
    busImage,
  });
  try {
    const bus = await buses.save();
  } catch (err) {
    next(response(errorCode.internal_server_error, "Internal server error"));
  }
  const payload = buses.id;
  res.status(errorCode.ok).send(payload);
};

exports.viewBuses = async (req, res, next) => {
  const { userId } = req.body;
  try {
    const buses = await Buses.find({ userId: userId });
    if (buses) {
      res.status(errorCode.ok).send(buses);
    } else {
      next(response(errorCode.bad_request, "buses cannot be viewed"));
    }
  } catch (error) {
    next(response(errorCode.internal_server_error, "intenal server error"));
  }
};

exports.removeBuses = async (req, res, next) => {
  const { busId } = req.body;
  try {
    const buses = await Buses.findByIdAndRemove(busId);
    if (buses) {
      res.status(errorCode.ok).send({});
    } else {
      next(response(errorCode.bad_request, "Buses cannot be deleted"));
    }
  } catch (error) {
    next(response(errorCode.internal_server_error, "Internal server error"));
  }
};

exports.resetSeats = async (req, res, next) => {
  let seatDetails = await seatData.seatDetails();
  const { busId } = req.body;
  try {
    const buses = await Buses.findByIdAndUpdate(busId, {
      $set: { seatDetails: seatDetails },
    });
    if (buses) {
      res.status(errorCode.ok).send({});
    } else {
      next(response(errorCode.bad_request, "seat cannot be reseted"));
    }
  } catch (error) {
    next(response(errorCode.internal_server_error, "Internal server error"));
  }
};
