const Buses = require("../model/buses");
const Ticket = require("../model/ticket");
const shortid = require("shortid");
var response = require("../middleware/response");
var nodemailer = require("nodemailer");
const errorCode = require("../utils/errorCodes");
errorCode.internal_server_error;

exports.getBuses = async (req, res, next) => {
  const { sourceCity, destinationCity, travelDate } = req.body;
  Buses.find({
    sourceCity: sourceCity,
    destinationCity: destinationCity,
    travelDate: travelDate,
  })
    .then((result) => {
      res.status(errorCode.ok).send(result);
    })
    .catch((err) => {
      next(response(errorCode.bad_request, "Buses cannot be retrieved"));
    });
};

exports.bookSeats = async (req, res, next) => {
  const {
    seatId,
    userName,
    email,
    mobileNumber,
    age,
    busId,
    gender,
  } = req.body;
  try {
    for (var i = 0; i < seatId.length; i++) {
      let ticketId = await shortid.generate();
      let row = seatId[i] - 1;
      let st = "seatDetails." + row;
      var result = await Buses.findOneAndUpdate(
        { _id: busId },
        {
          $set: {
            [`${st + ".isReserved"}`]: true,
            [`${st + ".ticketId"}`]: ticketId,
            [`${st + ".email"}`]: email,
            [`${st + ".userName"}`]: userName,
            [`${st + ".mobileNumber"}`]: mobileNumber,
            [`${st + ".age"}`]: age,
            [`${st + ".gender"}`]: gender,
          },
        }
      );
      let ticket = new Ticket({
        ticketId: ticketId,
        seatId: seatId[i],
        email: email,
        name: userName,
        mobileNumber: mobileNumber,
        age: age,
        gender: gender,
        busId: busId,
      });
      await ticket.save();
      var transporter = nodemailer.createTransport({
        service: "gmail",
        auth: {
          user: process.env.mailUser,
          pass: process.env.mailPass,
        },
      });

      var mailOptions = {
        from: "premvelu15@gmail.com",
        to: email,
        subject: "Thanks for booking with us",
        html:
          "<center><h1>Welcome " +
          userName +
          " </h1><h3>Your seat number " +
          seatId[i] +
          " has been booked successfully and Ticket Id is <b>" +
          ticketId +
          '</b> </h3><p>For more information visit <a href="http://localhost:3001/ticketStatus">here</a><h2>Thanks for booking with us!</h2><center>',
      };

      transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
          console.log(error);
        } else {
        }
      });
    }
    res.status(errorCode.ok).send(result);
  } catch (err) {
    next(
      response(errorCode.bad_request, "Passenger details cannot be updtaed")
    );
  }
};

exports.ticketDetails = async (req, res, next) => {
  const { ticketId } = req.body;
  let result = Ticket.find({ ticketId: ticketId })
    .then(async function (err, result) {
      res.status(errorCode.ok).send(result);
    })
    .catch((err) => {
      next(
        response(errorCode.bad_request, "Ticket details cannot be retrieved")
      );
    });
};

exports.busDetail = (req, res, next) => {
  const { busId } = req.body;
  Buses.findById(busId)
    .then(function (result) {
      res.status(errorCode.ok).send(result);
    })
    .catch((err) => {
      next(response(errorCode.bad_request, "Bus details cannot be retrieved"));
    });
};
