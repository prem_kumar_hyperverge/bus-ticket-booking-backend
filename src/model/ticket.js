const mongoose = require("mongoose");

const TicketSchema = mongoose.Schema({
    ticketId: {
    type: String
  },
  name: {
    type: String,
  },
  email: {
    type: String,
  },
  mobileNumber: {
    type: String,
  },
  age: {
    type: String,
  },
  createdAt: {
    type: Date,
    default: Date.now()
  },
  gender: {
    type: String,
  },
  busId: {
    type:String,
  },
  seatId: {
    type: String
  }
});


module.exports = mongoose.model("ticket", TicketSchema);