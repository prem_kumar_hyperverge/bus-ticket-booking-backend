const mongoose = require("mongoose");

const BusesSchema = mongoose.Schema({
    busName: {
    type: String,
    required: true
  },
  userId: {
    type: String,
    required: true
  },
  sourceCity: {
    type: String,
    required: true,
  },
  destinationCity: {
    type: String,
    required: true
  },
  fare: {
    type: String,
    required: true
  },
  createdAt: {
    type: Date,
    default: Date.now()
  },
  travelDate: {
    type: Array,
    default: []
  },
  departureTime: {
    type: String,
    required: true
  },
  arrivalTime: {
    type: String,
    required: true
  },
  noOfSeats: {
    type: String,
    required: true
  },
  pickupLocation: {
    type: String,
    required: true
  },
  busImage: {
    type: String,
    required: true
  },
  dropLocation: {
    type: String,
    required: true
  },
  busNumber:{
    type: String,
    required: true
  },  
  seatDetails: {
    type: Array,
    default: []
  }
});


module.exports = mongoose.model("buses", BusesSchema);