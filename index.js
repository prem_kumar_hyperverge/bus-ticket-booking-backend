const dotenv = require("dotenv");
dotenv.config();
var app = require("./src/app");
const InitiateMongoServer = require("./src/config/db");

InitiateMongoServer();

var port = process.env.PORT || 3333;

app.listen(port, () => {
  console.log("Bus Ticket Booking API is listening on port " + port);
});
